package br.com.novapat.acesso.consumer;

import br.com.novapat.acesso.models.Acesso;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Formatter;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-patricia-novaes-1", groupId = "PATI-NOVAES-1")
    public void receber(@Payload Acesso acesso) throws IOException,
            CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        System.out.println("Atenção, é: "+acesso.getTemAcesso()+
                           " o acesso cliente de id: " + acesso.getClienteId() +
                           " à porta de id:" + acesso.getPortaId()+
                           ". Data de verificação: " + LocalDateTime.now().format(formatter));

        Writer writer = Files.newBufferedWriter(Paths.get("acessos.csv"));
        StatefulBeanToCsv<Acesso> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
        beanToCsv.write(acesso);
        writer.flush();
        writer.close();

    }

}
