package br.com.novapat.acesso.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Acesso {

    @JsonIgnore
    private Long id;

    @JsonProperty("porta_id")
    private Long portaId;

    @JsonProperty("cliente_id")
    private Long clienteId;

    @JsonProperty("tem_acesso")
    private Boolean temAcesso = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public Long getPortaId() {
        return portaId;
    }

    public void setPortaId(Long portaId) {
        this.portaId = portaId;
    }

    public Boolean getTemAcesso() {
        return temAcesso;
    }

    public void setTemAcesso(Boolean temAcesso) {
        this.temAcesso = temAcesso;
    }
}
